/*Brandon DeVille
CS 3800 Homework 3
Main Javascript file*/

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var uuid = require('node-uuid');
var Room = require('./room.js');
var Person = require('./person.js');
var rooms = {};
var people = {};
var sockets = [];
var numRooms = 0;
var numPeople = 0;

function isEmpty(object){
	for(var key in object){
		if(object.hasOwnProperty(key)){
			return false;
		}
	}
	return true;
}

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

process.on('SIGINT', function() {
    console.log('Beginning server close');
    io.emit('message', "Server will shut down in 10 seconds.")
    setTimeout(function(){
      for(var i = 0; i < numPeople; i++){
      	io.to(sockets[i].id).emit('message', "You have now left the server")
      	sockets[i].disconnect();
      }
      io.emit('message', "Server Closed.")
      process.exit();
    }, 10000);
});

io.on('connection', function(socket){
  console.log('A user has connected');

  socket.on('disconnect', function(){
  	var id = socket.id;
    if(people[id] != null){
      if(isEmpty(people)){
        console.log('A user has disconnected');
      }
      else {
        console.log(people[id].name, ' has disconnected');
        for(var i = 0; i < numRooms; i++){
          for(var j = 0; j < rooms[i].userCount(); j++){
            if(rooms[i].users[j] == socket.id){
              io.to(rooms[i].name).emit('chat message', {user: people[id].name, msg: "has logged out."});
              rooms[i].numUsers-=1;
              delete rooms[i].users[j];
            }
          }
        }
        delete people[id];
      }
    }
    else{
      console.log('Unknown user has disconnected');
    }
  });

  socket.on('add user', function(data){
  	console.log(data, " has logged in")
  	var person = new Person(data, socket.id);
  	people[socket.id] = person;
  	sockets[numPeople] = socket;
  	numPeople+=1;
  	io.to(socket.id).emit('message', "Welcome to the Chat, please create a room or join an existing one.")
  });

  socket.on('create server', function(data){
  	var canMake = true;
  	if(data.user != null){
  	  for(var i = 0; i < numRooms; i++){
  		if(rooms[i].Name() == data.client){
  		  canMake = false;
  	    io.to(socket.id).emit('message', "Server already exists")
  	    io.to(socket.id).emit('message', "You will now be disconneted. Refreshing...")
  		  setTimeout(function(){
          io.to(socket.id).emit('message', "Refresh")
          socket.disconnect()
        }, 1500);
  		}
  	  }
  	  if(canMake){
  	  	console.log(data.user, " has created server: ", data.client)
        socket.join(data.client)
        var room = new Room(data.client, numRooms, socket.id);
        rooms[numRooms] = room;
        rooms[numRooms].addPerson(socket);
        numRooms+=1;
        io.to(data.client).emit('chat message', {user: data.user, msg: "is hosting this room"});
        console.log("There are", rooms[numRooms-1].userCount(), "user(s).")
  	  }
  	}
  	else{
  		io.to(socket.id).emit('message', "You cannot create a server without a username")
  		io.to(socket.id).emit('message', "You will now be disconneted. Refreshing...")
  		setTimeout(function(){
        io.to(socket.id).emit('message', "Refresh")
        socket.disconnect()
      }, 1500);
  	}
  })

  socket.on('join server', function(data){
  	var id = 0;
  	var Exists = false;
  	if(data.user != null){
  		for(var i = 0; i < numRooms; i++){
  		  if(rooms[i].Name() == data.client){
  			id = i;
  			Exists = true;
  		  }
  	    }
  	    if(!Exists){
  	    	io.to(socket.id).emit('message', "Room does not exist.");
  			  io.to(socket.id).emit('message', "You will now be disconneted. Refreshing...")
          setTimeout(function(){
            io.to(socket.id).emit('message', "Refresh")
            socket.disconnect()
          }, 1500);
  	    }
  		else if(rooms[id].isOpen()){
  			console.log(data.user, " has joined server: ", data.client)
      		socket.join(data.client)
      		rooms[id].addPerson(socket);
      		socket.broadcast.to(data.client).emit('chat message', {user: data.user, msg: "has logged in."});
      		console.log("There are", rooms[id].userCount(), "user(s).")
  		}
  		else{
  			io.to(socket.id).emit('message', "Room is full, you cannot join.");
  			io.to(socket.id).emit('message', "You will now be disconneted. Refreshing...")
        setTimeout(function(){
          io.to(socket.id).emit('message', "Refresh")
          socket.disconnect()
        }, 1500);
  		}
  	}
  	else{
  		io.to(socket.id).emit('message', "You cannot join a server without a username")
  		io.to(socket.id).emit('message', "You will now be disconneted. Refreshing...")
      setTimeout(function(){
        io.to(socket.id).emit('message', "Refresh")
        socket.disconnect()
      }, 1500);
  	}
  });

  socket.on("list servers", function(data) {
  	if(isEmpty(rooms)){
  		io.to(socket.id).emit('message', "There are no rooms")
  	}
  	else{
      for(var i = 0; i < numRooms; i++){
  	    io.to(socket.id).emit('message', rooms[i].Name())
  	  }
  	}
  });

  socket.on('chat message', function(data){
  	if(data.client != null && data.user != null){
  	  if(data.msg == "/exit" || data.msg == "/quit" || data.msg == "/part"){
  	  	var id = 0;
        for(var i = 0; i < numRooms; i++){
  		    if(rooms[i].Name() == data.client){
  		      id = i;
  		    }
  	    }
  	    if(rooms[id].owner == socket.id){
  		    io.to(data.client).emit('message', "Room will close in 10 seconds");
  		    setTimeout(function(){
  		  	  rooms[id].sockets.forEach(function(s){
  		      	s.leave(rooms[id].name);
  		  	    io.to(socket.id).emit('message', "Refresh")
  		        s.disconnect();
  		      })
  		 	    delete rooms[id];
  		   	  numRooms-=1;
  		    }, 10000);
      	}
     	  else{
  	      socket.broadcast.to(rooms[id].name).emit('chat message', {user: data.user, msg: "has logged out."})
  	  	  console.log(data.user, "has left server:", rooms[id].name)
  	  	  socket.leave(rooms[id].name);
  	  	  io.to(socket.id).emit('message', "Refresh")
  	  	  socket.disconnect();
  	    }
  	  }
  	  else{
  	  	console.log(data.user, ' sent message: "', data.msg, '" on server: ', data.client);
      	io.to(data.client).emit('chat message', {user: data.user, msg: data.msg});
  	  }
  	}
  	else{
  		io.to(socket.id).emit('message', "Error, Server or Username not valid.")
  	}
  });
});

http.listen(3001, function(){
  console.log('listening on *:3001');
});