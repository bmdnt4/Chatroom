/*Brandon DeVille
CS 3800 Homework 3
JavaScript file for Rooms*/

function Room(name, id, owner) {
  this.name = name;
  this.id = id;
  this.owner = owner;
  this.users = {};
  this.numUsers = 0;
  this.sockets = [];
};

Room.prototype.userCount = function() {
  return this.numUsers;
};

Room.prototype.addPerson = function(data) {
  this.users[this.numUsers] = data.id;
  this.sockets.push(data);
  this.numUsers++;
};

Room.prototype.isOpen = function() {
  return this.numUsers < 10;
};

Room.prototype.Name = function() {
  return this.name;
};

module.exports = Room;